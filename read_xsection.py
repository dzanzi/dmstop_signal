#!/usr/bin/env python

import math
import collections
import json

# Nev to be requested for given lumi, in blocks of 10k, rounding higher
# Nev = xsec * eff * bf * lumi
def getNev(xsec, eff, bf, lumi): # pb, rel, rel, fb-1
    nev = int(math.ceil(xsec * eff * bf * lumi / 10) * 10000)
    return nev

# Return Nev as blocks of 10k events
# Mc16a/d/e target 180/220/300 ifb luminosity
def getNevAll(xsec, eff, bf=1.): # pb, rel, rel
    nev_mc16a = getNev(xsec, eff, bf, 180)
    nev_mc16d = getNev(xsec, eff, bf, 220)
    nev_mc16e = getNev(xsec, eff, bf, 300)
    return nev_mc16a, nev_mc16d, nev_mc16e

if __name__ == "__main__":

    with open('sort_cross_sections.txt', 'r') as reader:
        input_lines = reader.readlines()

    physicsShort = {}
    xsection = {}
    xsection_NLO = {}
    filterEff = {}
    Nevs = {}
    Mass1 = {}
    Mass2 = {}

    for line in input_lines:
	DSID = line.split('/')[0]
	if "physicsShort" in line:
		phys = line.split('OK: ')[1]
		phys = phys.split(' physicsShort')[0]
		physicsShort.update({DSID: str(phys)})
                mass1 = str(phys.split('_')[4])
                mass1 = mass1.replace('p', '')
                mass2 = str(phys.split('_')[5])
		mass2 = mass2.replace('c', '')
		mass2 = mass2.replace('p', '.')
		Mass1.update({DSID: float(mass1)})
		Mass2.update({DSID: float(mass2)})
	elif "cross-section" in line:
		xsec = line.split('(nb)= ')[1]
		xsec = 1000.*float(xsec.split('\n')[0]) # nb -> pb
		xsection.update({DSID: xsec})
		xsection_NLO.update({DSID: xsec}) 
        elif "GenFiltEff" in line:
                EF = line.split('GenFiltEff = ')[1]
		EF = EF.split('\n')[0]
                filterEff.update({DSID: float(EF)})
	elif "_NLO" in line:
		#DSID = DSID.replace("_NLO","")
		line_new = line.split('/')[1]
		phys = line_new.split('.')[0]
                mass1 = str(phys.split('_')[4])
                mass1 = mass1.replace('p', '')
                mass2 = str(phys.split('_')[5])
                mass2 = mass2.replace('c', '')
                mass2 = mass2.replace('p', '.')
                xsec = line_new.split('Total cross section:')[1] # NLO xs in pb
                xsec = xsec.split(' +- ')[0]
		if DSID in xsection:
			xsection[DSID] = float(xsec)
		else:
			physicsShort.update({DSID: str(phys)})
                	Mass1.update({DSID: float(mass1)})
                	Mass2.update({DSID: float(mass2)})
                	xsection.update({DSID: float(xsec)})

    for DSID1 in physicsShort:
	if not ("NLO" in DSID1):
		continue
	for DSID2 in physicsShort:
		if not ("DM" in physicsShort[DSID2]):
		    continue
		filt = "_"+physicsShort[DSID2].split("_")[6]
		phys2 = physicsShort[DSID2].replace(filt,"")
		phys1 = physicsShort[DSID1].replace("_NLO","")
		if phys1==phys2:
			xsection_NLO[DSID2] = xsection[DSID1]/xsection[DSID2]

    with open('pp13_stopsbottom_NNLO_NNLL.json') as json_file:
        data = json.load(json_file)
    
    for DSID in physicsShort: 
       if not ("directTT" in physicsShort[DSID] or "bWN" in physicsShort[DSID]):
	  continue
       found = False
       for p in data['data']:
           if float(Mass1[DSID]) == float(p):
               xsection[DSID] = data['data'][p]['xsec_pb']
	       xsection_NLO[DSID] = data['data'][p]['xsec_pb']
               found = True
       if not found:
           print("Xsec for mass "+str(Mass1[DSID])+" not found")


    for DSID in physicsShort:
        if "NLO" in physicsShort[DSID]:
                continue
	if not(DSID in xsection):
		continue
	Nevs.update({DSID: getNevAll(xsection[DSID], filterEff[DSID], 1.)})

    physicsShort = collections.OrderedDict(sorted(physicsShort.items()))    
    xsection = collections.OrderedDict(sorted(xsection.items()))
    filterEff = collections.OrderedDict(sorted(filterEff.items()))
    Nevs = collections.OrderedDict(sorted(Nevs.items()))
    Mass1 = collections.OrderedDict(sorted(Mass1.items()))
    Mass2 = collections.OrderedDict(sorted(Mass2.items()))

    with open('final_xsection.txt', 'w') as writer:
	for DSID in physicsShort:
		if "NLO" in physicsShort[DSID]:
                	continue
		if not(DSID in xsection):
                	continue
		line = str(DSID)+"\t"+physicsShort[DSID]+"\t"+str(Mass1[DSID])+"\t"+str(Mass2[DSID])+"\t"+str(xsection[DSID])+"\t"+str(xsection_NLO[DSID])+"\t"+str(filterEff[DSID])+"\t"+str(Nevs[DSID][0])+"\t"+str(Nevs[DSID][1])+"\t"+str(Nevs[DSID][2])+"\n"
		writer.write(line)



