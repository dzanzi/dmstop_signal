from MadGraphControl.MadGraphUtils import *

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
model_string = phys_short.split('_')[3]
mphi = phys_short.split('_')[4].replace("p","")
mchi = phys_short.split('_')[5].replace("c","")
if "p" in str(mchi):
    mchi = str(mchi).replace("p",".")
mphi = float(mphi)
mchi = float(mchi)

evgenLog.info('Processing model with masses: (mphi, mchi) = (%e,%e)' %(mphi, mchi))

if "ttscalar" in phys_short.lower():
    gen_process = """
import model DMsimp_s_spin0 --modelname
define p = g u c d s u~ c~ d~ s~ b b~ 
define j = g u c d s u~ c~ d~ s~ b b~ 
generate p p > xd xd~ t t~ / a z w+ w- [QCD]
output -f 
launch 
2 
done 
set parton_shower PYTHIA8 
set pdlabel lhapdf 
set lhaid 260000 
set ebeam1 6500 
set ebeam2 6500 
set nevents 10000 
set MXd  %e 
set MY0  %e 
set DECAY  54 AUTO 
set gsxd 1.0 
set gsd11 0.0
set gsu11 0.0
set gsd22 0.0
set gsu22 0.0
set gsd33 1.0
set gsu33 1.0
set gpxd 0.0 
set gpd11 0.0
set gpu11 0.0
set gpd22 0.0
set gpu22 0.0
set gpd33 0.0
set gpu33 0.0
set gsh1 0.0
set gsb 0.0
set maxjetflavor 5 
set ptj 20 
set etaj 5 
set reweight_pdf True 
done 
"""%(mchi, mphi)
elif "ttpseudo" in phys_short.lower():
    gen_process = """
import model DMsimp_s_spin0 --modelname
define p = g u c d s u~ c~ d~ s~ b b~ 
define j = g u c d s u~ c~ d~ s~ b b~ 
generate p p > xd xd~ t t~ / a z w+ w- [QCD]
output -f 
launch 
2 
done 
set parton_shower PYTHIA8 
set pdlabel lhapdf 
set lhaid 260000 
set ebeam1 6500 
set ebeam2 6500 
set nevents 10000 
set MXd  %e 
set MY0  %e 
set DECAY  54 AUTO
set gsxd 0.0 
set gsd11 0.0
set gsu11 0.0
set gsd22 0.0
set gsu22 0.0
set gsd33 0.0
set gsu33 0.0
set gpxd 1.0 
set gpd11 0.0
set gpu11 0.0
set gpd22 0.0
set gpu22 0.0
set gpd33 1.0
set gpu33 1.0
set gsh1 0.0
set gsb 0.0 
set maxjetflavor 5 
set ptj 20 
set etaj 5 
set reweight_pdf True 
done 
"""%(mchi, mphi)

# Set up the process
process_dir = new_process(gen_process)

# Generate the events
generate(process_dir=process_dir)


