#!/usr/bin/env python

import math
import collections
import json

# Nev to be requested for given lumi, in blocks of 10k, rounding higher
# Nev = xsec * eff * bf * lumi
def getNev(xsec, eff, bf, lumi): # pb, rel, rel, fb-1
    nev = int(math.ceil(xsec * eff * bf * lumi / 10) * 10000)
    return nev

# Return Nev as blocks of 10k events
# Mc16a/d/e target 180/220/300 ifb luminosity
def getNevAll(xsec, eff, bf=1.): # pb, rel, rel
    nev_mc16a = getNev(xsec, eff, bf, 180)
    nev_mc16d = getNev(xsec, eff, bf, 220)
    nev_mc16e = getNev(xsec, eff, bf, 300)
    return nev_mc16a, nev_mc16d, nev_mc16e

if __name__ == "__main__":

    with open('sort_cross_sections.txt', 'r') as reader:
        input_lines = reader.readlines()

    physicsShort = {}
    xsection = {}
    xsection_NLO = {}
    filterEff = {}
    Nevs = {}

    for line in input_lines:
	DSID = line.split('/')[0]
	if "physicsShort" in line:
		phys = line.split('OK: ')[1]
		phys = phys.split(' physicsShort')[0]
		physicsShort.update({DSID: str(phys)})
	elif "cross-section" in line:
		xsec = line.split('(nb)= ')[1]
		xsec = 1000.*float(xsec.split('\n')[0]) # nb -> pb
		xsection.update({DSID: xsec})
        elif "GenFiltEff" in line:
                EF = line.split('GenFiltEff = ')[1]
		EF = EF.split('\n')[0]
                filterEff.update({DSID: float(EF)})
    
    for DSID in physicsShort:
	Nevs.update({DSID: getNevAll(xsection[DSID], filterEff[DSID], 1.)})

    physicsShort = collections.OrderedDict(sorted(physicsShort.items()))    
    xsection = collections.OrderedDict(sorted(xsection.items()))
    filterEff = collections.OrderedDict(sorted(filterEff.items()))
    Nevs = collections.OrderedDict(sorted(Nevs.items()))

    with open('final_xsection.txt', 'w') as writer:
	for DSID in physicsShort:
		line = str(DSID)+"\t"+physicsShort[DSID]+"\t"+str(xsection[DSID])+"\t"+str(filterEff[DSID])+"\t"+str(Nevs[DSID][0])+"\t"+str(Nevs[DSID][1])+"\t"+str(Nevs[DSID][2])+"\n"
		writer.write(line)



