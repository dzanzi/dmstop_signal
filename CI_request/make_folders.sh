#!/bin/sh

DSID=100000

while read p; do

  rm -r ${DSID}
  mkdir ${DSID}
  cd ${DSID}

  cp ../MadGraphControl_ttnunu_EFT.py .
  cp ../run_EVT_CI.sh .
  cp ../${p} .

  old_p=${p}
  p="${p/mc./}"
  p="${p/.py/}"

  sed -i "s/ToBeReplaced/${p}/g" run_EVT_CI.sh

  echo "executable = run_EVT_CI.sh" >> ${p}".sub"
  echo "should_transfer_files = YES" >> ${p}".sub"
  echo "transfer_input_files = MadGraphControl_ttnunu_EFT.py,${old_p}" >> ${p}".sub"
  echo "output          = "${p}".\$(ClusterId).\$(ProcId).out" >> ${p}".sub"
  echo "error           = "${p}".\$(ClusterId).\$(ProcId).err" >> ${p}".sub"
  echo "log             = "${p}".\$(ClusterId).\$(ProcId).log" >> ${p}".sub"
  echo "universe = vanilla" >> ${p}".sub"
  echo '+JobFlavour      = "tomorrow"' >> ${p}".sub" # workday=8h, tomorrow=1day, testmatch=3days
  echo "queue" >> ${p}".sub"

  echo "JOB "${p}" "${p}".sub\n" >> ${p}".dag"

  condor_submit_dag -force ${p}.dag

  cd ..

  DSID=$((DSID+1))

done < newCISignals.txt

DSID=100000

rm -r prepare_CI_JOs
mkdir prepare_CI_JOs
cd prepare_CI_JOs

while read p; do

  mkdir ${DSID}
  cd ${DSID}

  if [ "${DSID}" -eq "100000" ]; then
     cp ../../MadGraphControl_ttnunu_EFT.py .
  else
     ln -s ../100000/MadGraphControl_ttnunu_EFT.py . 
  fi

  cp ../../${p} .

  cd ..
  DSID=$((DSID+1))

done < ../newCISignals.txt

cd ..

rm prepare_CI_JOs.tar.gz
tar czf prepare_CI_JOs.tar.gz prepare_CI_JOs

