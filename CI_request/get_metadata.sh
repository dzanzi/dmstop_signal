#!/bin/sh

lsetup  "asetup AthAnalysisBase,2.4.35" pyAMI

getMetadata.py --fields="ldn,dataset_number,crossSection,kFactor,genFiltEff,processGroup,crossSectionRef,crossSectionTotalRelUncertDOWN,crossSectionTotalRelUncertUP" --inDsTxt="new.txt" --outFile="metadata.txt" --delim=";"


