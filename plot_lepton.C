

void plot_lepton(){

	TFile file("/afs/cern.ch/user/d/dzanzi/EOS_dzanzi/SUSY/SignalTest/MGPy8EG_A14N23LO_DM_TTscalar_p500_c1_1LandLowMET.DAOD_TRUTH3.test.pool.root","r");

	TTree* tree(0);
	file.GetObject("CollectionTree",tree);

	if(!tree) return;
	
        TBranch        *b_TruthMuonsAuxDyn_px;   //!
	TBranch        *b_TruthMuonsAuxDyn_py;   //!
        TBranch        *b_TruthMuonsAuxDyn_pz;   //!
        TBranch        *b_TruthMuonsAuxDyn_m;   //!
	TBranch        *b_TruthElectronsAuxDyn_px;   //!
	TBranch        *b_TruthElectronsAuxDyn_py;   //!
        TBranch        *b_TruthElectronsAuxDyn_pz;   //!
        TBranch        *b_TruthElectronsAuxDyn_m;   //!
	TBranch	       *b_TruthMuonsAuxDyn_classifierParticleType;
        TBranch        *b_TruthElectronsAuxDyn_classifierParticleType;

	std::vector<float> * TruthMuonsAuxDyn_px=0;
        std::vector<float> * TruthMuonsAuxDyn_py=0;
        std::vector<float> * TruthMuonsAuxDyn_pz=0;
        std::vector<float> * TruthMuonsAuxDyn_m=0;
        std::vector<float> * TruthElectronsAuxDyn_px=0;
        std::vector<float> * TruthElectronsAuxDyn_py=0;
        std::vector<float> * TruthElectronsAuxDyn_pz=0;
        std::vector<float> * TruthElectronsAuxDyn_m=0;
	std::vector<unsigned int> * TruthMuonsAuxDyn_classifierParticleType=0;
        std::vector<unsigned int> * TruthElectronsAuxDyn_classifierParticleType=0;

        tree->SetBranchAddress("TruthMuonsAuxDyn.px", &TruthMuonsAuxDyn_px, &b_TruthMuonsAuxDyn_px);
        tree->SetBranchAddress("TruthMuonsAuxDyn.py", &TruthMuonsAuxDyn_py, &b_TruthMuonsAuxDyn_py);
        tree->SetBranchAddress("TruthMuonsAuxDyn.pz", &TruthMuonsAuxDyn_pz, &b_TruthMuonsAuxDyn_pz);
        tree->SetBranchAddress("TruthMuonsAuxDyn.m", &TruthMuonsAuxDyn_m, &b_TruthMuonsAuxDyn_m);
        tree->SetBranchAddress("TruthElectronsAuxDyn.px", &TruthElectronsAuxDyn_px, &b_TruthElectronsAuxDyn_px);
        tree->SetBranchAddress("TruthElectronsAuxDyn.py", &TruthElectronsAuxDyn_py, &b_TruthElectronsAuxDyn_py);
        tree->SetBranchAddress("TruthElectronsAuxDyn.pz", &TruthElectronsAuxDyn_pz, &b_TruthElectronsAuxDyn_pz);
        tree->SetBranchAddress("TruthElectronsAuxDyn.m", &TruthElectronsAuxDyn_m, &b_TruthElectronsAuxDyn_m);
	tree->SetBranchAddress("TruthMuonsAuxDyn.classifierParticleType", &TruthMuonsAuxDyn_classifierParticleType, &b_TruthMuonsAuxDyn_classifierParticleType);
	tree->SetBranchAddress("TruthElectronsAuxDyn.classifierParticleType", &TruthElectronsAuxDyn_classifierParticleType, &b_TruthElectronsAuxDyn_classifierParticleType);

	TH1F h_pt("h_pt","h_pt",100,0,100e3);
	TH1I h_nlep("nlep","nlep",10,0,10);

	int nentries = tree->GetEntries();
	cout<<nentries<<"\n";
	for(unsigned int entry=0;entry<nentries;entry++){
		tree->GetEntry(entry);
	
		float pt_max(0.);
		unsigned int nlep(0);
		for(unsigned int m=0; m<TruthMuonsAuxDyn_px->size();m++){
		//	if(TruthMuonsAuxDyn_classifierParticleType->at(m)!=6) continue;
			TLorentzVector tmp;
			tmp.SetXYZM(TruthMuonsAuxDyn_px->at(m),TruthMuonsAuxDyn_py->at(m),TruthMuonsAuxDyn_pz->at(m),TruthMuonsAuxDyn_m->at(m));
			if(fabs(tmp.Eta())>2.8) continue; 
			nlep++;
			if(pt_max<tmp.Pt()) pt_max=tmp.Pt();
		}
                for(unsigned int m=0; m<TruthElectronsAuxDyn_px->size();m++){ 
                //        if(TruthElectronsAuxDyn_classifierParticleType->at(m)!=2) continue;
                        TLorentzVector tmp;
                        tmp.SetXYZM(TruthElectronsAuxDyn_px->at(m),TruthElectronsAuxDyn_py->at(m),TruthElectronsAuxDyn_pz->at(m),TruthElectronsAuxDyn_m->at(m)); 
                        if(fabs(tmp.Eta())>2.8) continue;
			nlep++;
                        if(pt_max<tmp.Pt()) pt_max=tmp.Pt();
                }

		h_pt.Fill(pt_max);		
		h_nlep.Fill(nlep);
	}

	TCanvas can("c","c",800,600);
        h_pt.SetLineColor(kBlue);
        h_pt.Draw();
	can.Print("plots/lep_pt.eps");

        can.Clear();
        h_nlep.SetLineColor(kBlue);
        h_nlep.Draw();
        can.Print("plots/nlep.eps");

	file.Close();

	return;

	
}


