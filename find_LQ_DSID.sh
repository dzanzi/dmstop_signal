#!/bin/sh

# vector uptype LQ
END=502786
for ((i=502723;i<=END;i++)); do
    #echo $i
    #rucio ls --short mc15_13TeV.${i}.MGPy8EG_'*'.evgen.EVNT.e8245
done

# scalar uptype LQ
arr1=("310550" "310552" "310554" "310556" "310558" "310560")
for i in "${arr1[@]}"
do
   #echo ${i}
   rucio ls --short mc15_13TeV.${i}.'*'.evgen.EVNT.e6981
done

arr2=("312201" "312203" "312205" "312207" "312209" "312211" "312213"
                "312215" "312217" "312219" "312221" "312223" "312225" "312227" "312229" "312231" "312233" "312235" "312237" "312239" "312241")
for i in "${arr2[@]}"
do
   rucio ls --short mc15_13TeV.${i}.'*'.evgen.EVNT.e7536
done

