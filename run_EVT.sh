#!/bin/sh

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

ls

setupATLAS

asetup AthGeneration,21.6.42,here
Gen_tf.py --ecmEnergy=13000.0 --jobConfig=$PWD --outputEVNTFile=tmp.EVNT.root --maxEvents=500


asetup 21.2.86.0,AthDerivation,here
Reco_tf.py --inputEVNTFile tmp.EVNT.root --outputDAODFile test.pool.root --reductionConf TRUTH3


cp /afs/cern.ch/user/d/dzanzi/WORKDIR/SUSY/tt1L/newSignals/simpleAnalysis/build/.asetup.save .
asetup --restore # restores previous release
source /afs/cern.ch/user/d/dzanzi/WORKDIR/SUSY/tt1L/newSignals/simpleAnalysis/build/x86_64-centos7-gcc8-opt/setup.sh
simpleAnalysis  -a StopOneLepton2018 DAOD_TRUTH3.test.pool.root


cp tmp.EVNT.root /afs/cern.ch/user/d/dzanzi/EOS_dzanzi/SUSY/SignalTest/ToBeReplaced.EVNT.root
cp DAOD_TRUTH3.test.pool.root /afs/cern.ch/user/d/dzanzi/EOS_dzanzi/SUSY/SignalTest/ToBeReplaced.DAOD_TRUTH3.test.pool.root 
cp StopOneLepton2018.root /afs/cern.ch/user/d/dzanzi/EOS_dzanzi/SUSY/SignalTest/ToBeReplaced.StopOneLepton2018.root
cp StopOneLepton2018.txt /afs/cern.ch/user/d/dzanzi/EOS_dzanzi/SUSY/SignalTest/ToBeReplaced.StopOneLepton2018.txt

rm tmp.EVNT.root
rm DAOD_TRUTH3.test.pool.root
rm events.lhe*
rm tmp_LHE_events*

