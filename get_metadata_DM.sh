#!/bin/sh

lsetup  "asetup AthAnalysisBase,2.4.35" pyAMI

getMetadata.py --fields="ldn,dataset_number,crossSection,kFactor,genFiltEff,processGroup,crossSectionRef,crossSectionTotalRelUncertDOWN,crossSectionTotalRelUncertUP" --inDsTxt="new_DM.txt" --outFile="metadata_DM.txt" --delim=";"


