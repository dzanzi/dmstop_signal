Submit batch jobs for creation of EVT and TRUTH3 samples:

source make_folders.sh

- output dir set in run_EVT.sh
- input samples from new*Signals.txt

Get cross sections and filter efficiencies:

source grep_xsection.sh

python read_xsection.py

- produces final_xsection.txt with target event statistics to requests

