#!/bin/sh

#DSID=100000
#
#while read p; do
#
#  mkdir ${DSID}
#  cd ${DSID}
#
#  cp ../MadGraphControl_DMSimp_DM_TT_TW_TJ.py .
#  cp ../run_EVT.sh .
# 
#  echo "evt_multiplier = 55" > ${p}
#  echo "evgenConfig.nEventsPerJob = 1000" >> ${p}
#  echo 'include("MadGraphControl_DMSimp_DM_TT_TW_TJ.py")' >> ${p}
#  
#  old_p=${p}
#  p="${p/mc./}"
#  p="${p/.py/}"              
#
#  sed -i "s/ToBeReplaced/${p}/g" run_EVT.sh
#
#  echo "executable = run_EVT.sh" >> ${p}".sub"
#  echo "should_transfer_files = YES" >> ${p}".sub"
#  echo "transfer_input_files = MadGraphControl_DMSimp_DM_TT_TW_TJ.py,${old_p}" >> ${p}".sub"
#  echo "output          = "${p}".\$(ClusterId).\$(ProcId).out" >> ${p}".sub"
#  echo "error           = "${p}".\$(ClusterId).\$(ProcId).err" >> ${p}".sub"
#  echo "log             = "${p}".\$(ClusterId).\$(ProcId).log" >> ${p}".sub"
#  echo "universe = vanilla" >> ${p}".sub"
#  echo '+JobFlavour      = "testmatch"' >> ${p}".sub" # workday=8h, tomorrow=1day, testmatch=3days
#  echo "queue" >> ${p}".sub"
#
#  echo "JOB "${p}" "${p}".sub\n" >> ${p}".dag"
#
#  condor_submit_dag -force ${p}.dag
#  
#
#  cd ..
#  DSID=$((DSID+1))
#
#done < newDMSignals.txt

#DSID=100000
#
#cd prepare_DM_JOs
#
#while read p; do
#
#  mkdir ${DSID}
#  cd ${DSID}
#
#  if [ "${DSID}" -eq "100000" ]; then
#     cp ../../MadGraphControl_DMSimp_DM_TT_TW_TJ.py .
#  else
#     ln -s ../100000/MadGraphControl_DMSimp_DM_TT_TW_TJ.py . 
#  fi
#
#  cp ../../${DSID}/log.generate .
# 
#  echo "evt_multiplier = 55" > ${p}
#  echo "evgenConfig.nEventsPerJob = 1000" >> ${p}
#  echo 'include("MadGraphControl_DMSimp_DM_TT_TW_TJ.py")' >> ${p}
#  
#  cd ..
#  DSID=$((DSID+1))
#
#done < ../newDMSignals.txt
#
#cd ..

#DSID=200000
#
#while read p; do
#
#  mkdir ${DSID}_NLO
#  cd ${DSID}_NLO
#
#  cp ../MadGraphControl_DMSimp_DM_TT_TW_TJ_NLO.py .
#  cp ../run_EVT_only.sh .
#
#  echo "evt_multiplier = 55" > ${p}
#  echo "evgenConfig.nEventsPerJob = 1000" >> ${p}
#  echo 'include("MadGraphControl_DMSimp_DM_TT_TW_TJ_NLO.py")' >> ${p}
#
#  old_p=${p}
#  p="${p/mc./}"
#  p="${p/.py/}"
#  p=${p}_NLO
#
#  echo "executable = run_EVT_only.sh" >> ${p}".sub"
#  echo "should_transfer_files = YES" >> ${p}".sub"
#  echo "transfer_input_files = MadGraphControl_DMSimp_DM_TT_TW_TJ_NLO.py,${old_p}" >> ${p}".sub"
#  echo "output          = "${p}".\$(ClusterId).\$(ProcId).out" >> ${p}".sub"
#  echo "error           = "${p}".\$(ClusterId).\$(ProcId).err" >> ${p}".sub"
#  echo "log             = "${p}".\$(ClusterId).\$(ProcId).log" >> ${p}".sub"
#  echo "universe = vanilla" >> ${p}".sub"
#  echo '+JobFlavour      = "testmatch"' >> ${p}".sub" # workday=8h, tomorrow=1day, testmatch=3days
#  echo "queue" >> ${p}".sub"
#
#  echo "JOB "${p}" "${p}".sub\n" >> ${p}".dag"
#
#  condor_submit_dag -force ${p}.dag
#
#  cd ..
#  DSID=$((DSID+1))
#
#done < newDMSignals_NLO.txt

#DSID=300000

#while read p; do

#  rm -r ${DSID}
#  mkdir ${DSID}
#  cd ${DSID}
#
#  cp ../MadGraphControl_SimplifiedModel* .
#  cp ../run_EVT_stop.sh .
#
#  echo "include( 'MadGraphControl_SimplifiedModel_TT_directTT.py' )" > ${p}
#
#  old_p=${p}
#  p="${p/mc./}"
#  p="${p/.py/}"
#
#  sed -i "s/ToBeReplaced/${p}/g" run_EVT_stop.sh
#
#  echo "executable = run_EVT_stop.sh" >> ${p}".sub"
#  echo "should_transfer_files = YES" >> ${p}".sub"
#  echo "transfer_input_files = MadGraphControl_SimplifiedModel_TT_directTT.py,${old_p}" >> ${p}".sub"
#  echo "output          = "${p}".\$(ClusterId).\$(ProcId).out" >> ${p}".sub"
#  echo "error           = "${p}".\$(ClusterId).\$(ProcId).err" >> ${p}".sub"
#  echo "log             = "${p}".\$(ClusterId).\$(ProcId).log" >> ${p}".sub"
#  echo "universe = vanilla" >> ${p}".sub"
#  echo '+JobFlavour      = "workday"' >> ${p}".sub" # workday=8h, tomorrow=1day, testmatch=3days
#  echo "queue" >> ${p}".sub"
#
#  echo "JOB "${p}" "${p}".sub\n" >> ${p}".dag"
#
#  #condor_submit_dag -force ${p}.dag
#
#  cd ..

#  rm -r ${DSID}_Py83
#  mkdir ${DSID}_Py83
#  cd ${DSID}_Py83
#
#  cp ../MadGraphControl_SimplifiedModel* .
#  cp ../run_EVT_stop_Py83.sh .
#
#  echo "include( 'MadGraphControl_SimplifiedModel_TT_directTT.py' )" > ${old_p}
#
#  old_p=${old_p}
#  p="${p/mc./}"
#  p="${p/.py/}"
#  p=${p}_Py83
#
#  sed -i "s/ToBeReplaced/${p}/g" run_EVT_stop_Py83.sh
#
#  echo "executable = run_EVT_stop_Py83.sh" >> ${p}".sub"
#  echo "should_transfer_files = YES" >> ${p}".sub"
#  echo "transfer_input_files = MadGraphControl_SimplifiedModel_TT_directTT.py,${old_p}" >> ${p}".sub"
#  echo "output          = "${p}".\$(ClusterId).\$(ProcId).out" >> ${p}".sub"
#  echo "error           = "${p}".\$(ClusterId).\$(ProcId).err" >> ${p}".sub"
#  echo "log             = "${p}".\$(ClusterId).\$(ProcId).log" >> ${p}".sub"
#  echo "universe = vanilla" >> ${p}".sub"
#  echo '+JobFlavour      = "workday"' >> ${p}".sub" # workday=8h, tomorrow=1day, testmatch=3days
#  echo "queue" >> ${p}".sub"
#
#  echo "JOB "${p}" "${p}".sub\n" >> ${p}".dag"
#
#  condor_submit_dag -force ${p}.dag
#
#  cd ..

#  DSID=$((DSID+1))

#done < newStopSignals.txt


#DSID=100000
#
#cd prepare_stop_JOs
#
#while read p; do
#
#  mkdir ${DSID}
#  cd ${DSID}
#
#  if [ "${DSID}" -eq "100000" ]; then
#     cp ../../MadGraphControl_SimplifiedModel_TT_directTT.py .
#  else
#     ln -s ../100000/MadGraphControl_SimplifiedModel_TT_directTT.py . 
#  fi
#
#  echo "include( 'MadGraphControl_SimplifiedModel_TT_directTT.py' )" > ${p}
#
#  cd ..
#  DSID=$((DSID+1))
#
#done < ../newStopSignals.txt
#
#cd ..

#tar czf prepare_stop_JOs.tar.gz  prepare_stop_JOs
#tar czf prepare_DM_JOs.tar.gz prepare_DM_JOs

#DSID=400000
#
#while read p; do
#
#  rm -r ${DSID}
#  mkdir ${DSID}
#  cd ${DSID}
#
#  cp ../MadGraphControl_SimplifiedModel_TT_bWN.py .
#  cp ../run_EVT_stop_Py83.sh .
#  #cp ../run_EVT_stop_slc6.sh .
#  #cp ../slc6_wrapper.sh .
#
#  echo "include( 'MadGraphControl_SimplifiedModel_TT_bWN.py' )" > ${p}
#
#  old_p=${p}
#  p="${p/mc./}"
#  p="${p/.py/}"
#
#  sed -i "s/ToBeReplaced/${p}/g" run_EVT_stop_Py83.sh
#
#  echo "executable = run_EVT_stop_Py83.sh" >> ${p}".sub"
#  echo "should_transfer_files = YES" >> ${p}".sub"
#  echo "transfer_input_files = MadGraphControl_SimplifiedModel_TT_bWN.py,${old_p}" >> ${p}".sub"
#  echo "output          = "${p}".\$(ClusterId).\$(ProcId).out" >> ${p}".sub"
#  echo "error           = "${p}".\$(ClusterId).\$(ProcId).err" >> ${p}".sub"
#  echo "log             = "${p}".\$(ClusterId).\$(ProcId).log" >> ${p}".sub"
#  echo "universe = vanilla" >> ${p}".sub"
#  echo '+JobFlavour      = "workday"' >> ${p}".sub" # workday=8h, tomorrow=1day, testmatch=3days
#  echo "queue" >> ${p}".sub"
#
#  echo "JOB "${p}" "${p}".sub\n" >> ${p}".dag"
#
#  condor_submit_dag -force ${p}.dag
#
#  cd ..
#
#  DSID=$((DSID+1))
#
#done < newStopSignals_3b.txt
#
#DSID=100000
#
#rm -r prepare_stop_JOs_3b
#mkdir prepare_stop_JOs_3b
#cd prepare_stop_JOs_3b
#
#while read p; do
#
#  mkdir ${DSID}
#  cd ${DSID}
#
#  if [ "${DSID}" -eq "100000" ]; then
#     cp ../../MadGraphControl_SimplifiedModel_TT_bWN.py .
#  else
#     ln -s ../100000/MadGraphControl_SimplifiedModel_TT_bWN.py . 
#  fi
#
#  echo "include( 'MadGraphControl_SimplifiedModel_TT_bWN.py' )" > ${p}
#
#  cd ..
#  DSID=$((DSID+1))
#
#done < ../newStopSignals_3b.txt
#
#cd ..
#
#rm prepare_stop_JOs_3b.tar.gz
#tar czf prepare_stop_JOs_3b.tar.gz prepare_stop_JOs_3b

#DSID=500000
#
#while read p; do
#
#  rm -r ${DSID}
#  mkdir ${DSID}
#  cd ${DSID}
#
#  cp ../MadGraphControl_SimplifiedModel_TT_directTT.py .
#  cp ../run_EVT_stop.sh .
#  cp ../run_EVT_stop_slc6.sh .
#  cp ../slc6_wrapper.sh .
#
#  echo "include( 'MadGraphControl_SimplifiedModel_TT_directTT.py' )" > ${p}
#
#  old_p=${p}
#  p="${p/mc./}"
#  p="${p/.py/}"
#
#  sed -i "s/ToBeReplaced/${p}/g" run_EVT_stop_slc6.sh
#
#  echo "executable = run_EVT_stop_slc6.sh" >> ${p}".sub"
#  echo "should_transfer_files = YES" >> ${p}".sub"
#  echo "transfer_input_files = slc6_wrapper.sh,MadGraphControl_SimplifiedModel_TT_directTT.py,${old_p}" >> ${p}".sub"
#  echo "output          = "${p}".\$(ClusterId).\$(ProcId).out" >> ${p}".sub"
#  echo "error           = "${p}".\$(ClusterId).\$(ProcId).err" >> ${p}".sub"
#  echo "log             = "${p}".\$(ClusterId).\$(ProcId).log" >> ${p}".sub"
#  echo "universe = vanilla" >> ${p}".sub"
#  echo '+JobFlavour      = "tomorrow"' >> ${p}".sub" # workday=8h, tomorrow=1day, testmatch=3days
#  echo "queue" >> ${p}".sub"
#
#  echo "JOB "${p}" "${p}".sub\n" >> ${p}".dag"
#
#  condor_submit_dag -force ${p}.dag
#
#  cd ..
#
#  DSID=$((DSID+1))
#
#done < newStopSignals_extension.txt
#
#DSID=100000
#
#rm -r prepare_stop_JOs_extension
#mkdir prepare_stop_JOs_extension
#cd prepare_stop_JOs_extension
#
#while read p; do
#
#  mkdir ${DSID}
#  cd ${DSID}
#
#  if [ "${DSID}" -eq "100000" ]; then
#     cp ../../MadGraphControl_SimplifiedModel_TT_directTT.py .
#  else
#     ln -s ../100000/MadGraphControl_SimplifiedModel_TT_directTT.py . 
#  fi
#
#  echo "include( 'MadGraphControl_SimplifiedModel_TT_directTT.py' )" > ${p}
#
#  cd ..
#  DSID=$((DSID+1))
#
#done < ../newStopSignals_extension.txt
#
#cd ..
#
#rm prepare_stop_JOs_extension.tar.gz 
#tar czf prepare_stop_JOs_extension.tar.gz  prepare_stop_JOs_extension


DSID=600000

while read p; do

  rm -r ${DSID}
  mkdir ${DSID}
  cd ${DSID}

  cp ../MadGraphControl_SimplifiedModel_TT_directTT.py .
  cp ../run_EVT_stop.sh .
  cp ../run_EVT_stop_slc6.sh .
  cp ../slc6_wrapper.sh .

  echo "evgenConfig.nEventsPerJob = 1000" >> ${p}
  echo "include( 'MadGraphControl_SimplifiedModel_TT_directTT.py' )" > ${p}

  old_p=${p}
  p="${p/mc./}"
  p="${p/.py/}"

  sed -i "s/ToBeReplaced/${p}/g" run_EVT_stop_slc6.sh
  sed -i "s/ToBeReplaced/${p}/g" slc6_wrapper.sh

  echo "executable = run_EVT_stop_slc6.sh" >> ${p}".sub"
  echo "should_transfer_files = YES" >> ${p}".sub"
  echo "transfer_input_files = slc6_wrapper.sh,MadGraphControl_SimplifiedModel_TT_directTT.py,${old_p}" >> ${p}".sub"
  echo "output          = "${p}".\$(ClusterId).\$(ProcId).out" >> ${p}".sub"
  echo "error           = "${p}".\$(ClusterId).\$(ProcId).err" >> ${p}".sub"
  echo "log             = "${p}".\$(ClusterId).\$(ProcId).log" >> ${p}".sub"
  echo "universe = vanilla" >> ${p}".sub"
  echo '+JobFlavour      = "tomorrow"' >> ${p}".sub" # workday=8h, tomorrow=1day, testmatch=3days
  echo "queue" >> ${p}".sub"

  echo "JOB "${p}" "${p}".sub\n" >> ${p}".dag"

  condor_submit_dag -force ${p}.dag

  cd ..

  DSID=$((DSID+1))

done < totStopSignals_short.txt

#DSID=610000
#
#while read p; do
#
#  rm -r ${DSID}
#  mkdir ${DSID}
#  cd ${DSID}
#
#  cp ../MadGraphControl_SimplifiedModel_TT_directTT.py .
#  cp ../run_EVT_DAOD.sh .
#
#  echo "include( 'MadGraphControl_SimplifiedModel_TT_directTT.py' )" > ${p}
#
#  old_p=${p}
#  p="${p/mc./}"
#  p="${p/.py/}"
#
#  sed -i "s/ToBeReplaced/${p}/g" run_EVT_DAOD.sh
#
#  echo "executable = run_EVT_DAOD.sh" >> ${p}".sub"
#  echo "should_transfer_files = YES" >> ${p}".sub"
#  echo "transfer_input_files = MadGraphControl_SimplifiedModel_TT_directTT.py,${old_p}" >> ${p}".sub"
#  echo "output          = "${p}".\$(ClusterId).\$(ProcId).out" >> ${p}".sub"
#  echo "error           = "${p}".\$(ClusterId).\$(ProcId).err" >> ${p}".sub"
#  echo "log             = "${p}".\$(ClusterId).\$(ProcId).log" >> ${p}".sub"
#  echo "universe = vanilla" >> ${p}".sub"
#  echo '+JobFlavour      = "tomorrow"' >> ${p}".sub" # workday=8h, tomorrow=1day, testmatch=3days
#  echo "queue" >> ${p}".sub"
#
#  echo "JOB "${p}" "${p}".sub\n" >> ${p}".dag"
#
#  condor_submit_dag -force ${p}.dag
#
#  cd ..
#
#  DSID=$((DSID+1))
#
#done < totStopSignals_short.txt


DSID=700000

while read p; do

  rm -r ${DSID}
  mkdir ${DSID}
  cd ${DSID}

  cp ../MadGraphControl_SimplifiedModel_TT_bWN.py .
  cp ../run_EVT_stop.sh .
  cp ../run_EVT_stop_slc6.sh .
  cp ../slc6_wrapper.sh .

  echo "include( 'MadGraphControl_SimplifiedModel_TT_bWN.py' )" > ${p}

  old_p=${p}
  p="${p/mc./}"
  p="${p/.py/}"

  sed -i "s/ToBeReplaced/${p}/g" run_EVT_stop.sh

  echo "executable = run_EVT_stop.sh" >> ${p}".sub"
  echo "should_transfer_files = YES" >> ${p}".sub"
  echo "transfer_input_files = slc6_wrapper.sh,MadGraphControl_SimplifiedModel_TT_bWN.py,${old_p}" >> ${p}".sub"
  echo "output          = "${p}".\$(ClusterId).\$(ProcId).out" >> ${p}".sub"
  echo "error           = "${p}".\$(ClusterId).\$(ProcId).err" >> ${p}".sub"
  echo "log             = "${p}".\$(ClusterId).\$(ProcId).log" >> ${p}".sub"
  echo "universe = vanilla" >> ${p}".sub"
  echo '+JobFlavour      = "tomorrow"' >> ${p}".sub" # workday=8h, tomorrow=1day, testmatch=3days
  echo "queue" >> ${p}".sub"

  echo "JOB "${p}" "${p}".sub\n" >> ${p}".dag"

  condor_submit_dag -force ${p}.dag

  cd ..

  DSID=$((DSID+1))

done < totStopSignals3b_short.txt

DSID=800000

while read p; do

  rm -r ${DSID}
  mkdir ${DSID}
  cd ${DSID}

  cp ../MadGraphControl_DMSimp_DM_TT_TW_TJ.py .
  cp ../run_EVT_DM_slc6.sh .
  cp ../slc6_wrapper_DM.sh .
 
  echo "evt_multiplier = 60" > ${p}
  echo "evgenConfig.nEventsPerJob = 1000" >> ${p}
  echo 'include("MadGraphControl_DMSimp_DM_TT_TW_TJ.py")' >> ${p}
  
  old_p=${p}
  p="${p/mc./}"
  p="${p/.py/}"              

  sed -i "s/ToBeReplaced/${p}/g" run_EVT_DM_slc6.sh
  sed -i "s/ToBeReplaced/${p}/g" slc6_wrapper_DM.sh

  echo "executable = run_EVT_DM_slc6.sh" >> ${p}".sub"
  echo "should_transfer_files = YES" >> ${p}".sub"
  echo "transfer_input_files = slc6_wrapper_DM.sh,MadGraphControl_DMSimp_DM_TT_TW_TJ.py,${old_p}" >> ${p}".sub"
  echo "output          = "${p}".\$(ClusterId).\$(ProcId).out" >> ${p}".sub"
  echo "error           = "${p}".\$(ClusterId).\$(ProcId).err" >> ${p}".sub"
  echo "log             = "${p}".\$(ClusterId).\$(ProcId).log" >> ${p}".sub"
  echo "universe = vanilla" >> ${p}".sub"
  echo '+JobFlavour      = "testmatch"' >> ${p}".sub" # workday=8h, tomorrow=1day, testmatch=3days
  echo "queue" >> ${p}".sub"

  echo "JOB "${p}" "${p}".sub\n" >> ${p}".dag"

  condor_submit_dag -force ${p}.dag
  

  cd ..
  DSID=$((DSID+1))

done < totDMSignals_short.txt























