# Simple variable setups
param_blocks = {} # For general params
masses = {}
decays = {}

# Useful definitions

# Basic settings for production and filters
madspin_card = None
param_card = None # Only set if you *can't* just modify the default param card to get your settings (e.g. pMSSM)

# Default run settings
run_settings = {
    'event_norm'    : 'average',
    'lhe_version'   : '3.0',
    'ickkw'         : 0,
    'parton_shower' : 'PYTHIA8',
    'req_acc'       : '0.001',
    'bwcutoff'      : '50',
    'dynamical_scale_choice' : '10', #user-defined scale -> Dominic's definition of mt+1/2*(pt^2+ptx^2)
} 

# Set up default PDF and systematic settings (note: action in import module)
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

# Event multipliers for getting more events out of madgraph to feed through athena (esp. for filters)
evt_multiplier=1.2

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

###############################################################
from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()
#get the file name and split it on _ to extract relavent information                                                                                              
jobConfigParts = JOName.split("_")

masses['6'] = 172.5  
masses['24'] = 80.3

plugin=None

process = '''
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
generate    p p > t~ t   [QCD]
'''

######################################################################################

from MadGraphControl.MadGraphUtils import * 

# Set maximum number of events if the event multiplier has been modified
if evt_multiplier>0:
    if runArgs.maxEvents>0:
        nevts=runArgs.maxEvents*evt_multiplier
    else:
        nevts=evgenConfig.nEventsPerJob*evt_multiplier
else:
    # Sensible default
    nevts=evgenConfig.nEventsPerJob*2.
run_settings.update({'nevents':int(nevts)})

# Pass arguments as a dictionary: the "decays" argument is not accepted in older versions of MadGraphControl
if 'mass' in [x.lower() for x in param_blocks]:
    raise RuntimeError('Do not provide masses in param_blocks; use the masses variable instead')
param_blocks['MASS']=masses
# Add decays in if needed
if len(decays)>0: param_blocks['DECAY']=decays

full_proc = SUSY_process(process)
process_dir = new_process(full_proc, plugin=plugin, usePMGSettings=True)
modify_param_card(param_card_input=param_card,process_dir=process_dir,params=param_blocks)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)

#config_setting = {}
#config_setting.update({'output_dependencies':'external'})
#config_setting.update({'ninja':'/afs/cern.ch/work/d/dzanzi/private/SUSY/tt1L/newSignals/dmstop_signal_v3/leftStop/LocalMadGraph/HEPTools/lib'})
#config_setting.update({'collier':'/afs/cern.ch/work/d/dzanzi/private/SUSY/tt1L/newSignals/dmstop_signal_v3/leftStop/LocalMadGraph/HEPTools/lib'})
#config_setting.update({'run_mode':'2'})
#config_setting.update({'nb_core':'None'})
#modify_config_card(process_dir=process_dir,settings=config_setting)

# Cook the setscales file for the user defined dynamical scale
import fileinput
fileN = process_dir+'/SubProcesses/setscales.f'
mark  = '      elseif(dynamical_scale_choice.eq.10) then'
rmLines = ['ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
           'cc      USER-DEFINED SCALE: ENTER YOUR CODE HERE                                 cc',
           'cc      to use this code you must set                                            cc',
           'cc                 dynamical_scale_choice = 0                                    cc',
           'cc      in the run_card (run_card.dat)                                           cc',
           'write(*,*) "User-defined scale not set"',
           'stop 1',
           'temp_scale_id=\'User-defined dynamical scale\' ! use a meaningful string',
           'tmp = 0',
           'cc      USER-DEFINED SCALE: END OF USER CODE                                     cc'
           ]

for line in fileinput.input(fileN, inplace=1):
    toKeep = True
    for rmLine in rmLines:
        if line.find(rmLine) >= 0:
           toKeep = False
           break
    if toKeep:
        print(line),
    if line.startswith(mark):
        print("""
c         Q^2= mt^2 + 0.5*(pt^2+ptbar^2)
          xm2=dot(pp(0,3),pp(0,3))
          tmp=sqrt(xm2+0.5*(pt(pp(0,3))**2+pt(pp(0,4))**2))
          temp_scale_id='mt**2 + 0.5*(pt**2+ptbar**2)'
              """)



# Set up madspin if needed
if madspin_card is not None:
    import shutil
    if not os.access(madspin_card,os.R_OK):
        raise RuntimeError('Could not locate madspin card at '+str(madspin_card))
    shutil.copy(madspin_card,process_dir+'/Cards/madspin_card.dat')

# Grab the run card and move it into place
generate(runArgs=runArgs,process_dir=process_dir,grid_pack=False)

# Move output files into the appropriate place, with the appropriate name
arrange_output(process_dir=process_dir,saveProcDir=False,runArgs=runArgs,fixEventWeightsForBridgeMode=False)

# Check if we were running multi-core, and if so move back to single core for Pythia8
check_reset_proc_number(opts)

write_test_script()

evgenConfig.contact  = [ "daniele.zanzi@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel','stop']
evgenConfig.description = 'stop+C1 production, st->b+C1 in simplified model'
evgenConfig.keywords += ["SUSY"]

