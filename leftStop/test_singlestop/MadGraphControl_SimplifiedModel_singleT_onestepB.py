# Simple variable setups
param_blocks = {} # For general params
decoupled_mass = '4.5E9'
masses = {}
for p in ['1000001','1000002','1000003','1000004','1000005','1000006','2000001','2000002','2000003','2000004','2000005','2000006','1000021',\
          '1000023','1000024','1000025','1000011','1000013','1000015','2000011','2000013','2000015','1000012','1000014','1000016','1000022',\
          '1000035','1000037','35','36','37']: # Note that gravitino is non-standard
    masses[p]=decoupled_mass
decays = {}

# Useful definitions
squarks = []
squarksl = []
for anum in [1,2,3,4]:
    squarks += [str(1000000+anum),str(-1000000-anum),str(2000000+anum),str(-2000000-anum)]
    squarksl += [str(1000000+anum),str(-1000000-anum)]

# Basic settings for production and filters
madspin_card = None
param_card = None # Only set if you *can't* just modify the default param card to get your settings (e.g. pMSSM)

# Set up default PDF and systematic settings (note: action in import module)
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

# Event multipliers for getting more events out of madgraph to feed through athena (esp. for filters)
evt_multiplier=1.2

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

###############################################################
from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()
#get the file name and split it on _ to extract relavent information                                                                                              
jobConfigParts = JOName.split("_")

mstop=float(jobConfigParts[4])
mchargino=float(jobConfigParts[5])
mneutralino=float(jobConfigParts[6].split('.')[0])

masses['6'] = 172.5
masses['24'] = 80.3
masses['1'] = 0.
masses['2'] = 0.
masses['3'] = 0.
masses['4'] = 0.
masses['5'] = 0.
masses['11'] = 0.
masses['13'] = 0.
masses['1000006'] = mstop
masses['1000024'] = mchargino
masses['1000022'] = mneutralino
if masses['1000022']<0.5: masses['1000022']=0.5

# Default run settings
run_settings = {
    'event_norm'    : 'average',
    'lhe_version'   : '3.0',
    'ickkw'         : 0,
    'parton_shower' : 'PYTHIA8',
    'req_acc'       : '0.001',
    'bwcutoff'      : '50',
    'dynamical_scale_choice' : '3',
    #'dynamical_scale_choice': '-1',
    #'fixed_fac_scale': True,
    #'fixed_ren_scale': True,
    #'muR_ref_fixed'  : mstop, 
    #'muF_ref_fixed'  : mstop, 
}

plugin=None
if 'STR' in JOName:
    plugin='MadSTR'
    # TODO: set these properly
    run_settings.update({
      'istr': '2',
      'str_include_pdf':  'True',
      'str_include_flux': 'True',
    })


process = '''
   import model MSSMatNLO_UFO-full --modelname
   define p = g u c d s b u~ c~ d~ s~ b~
   define j = g u c d s b u~ c~ d~ s~ b~
   generate    p p > x1- stl   [QCD] / go sul scl sur scr sdl ssl sbl sdr ssr sbr sul~ scl~ sur~ scr~ sdl~ ssl~ sbl~ sdr~ ssr~ sbr~ sne snm snt sel- smul- stau1- ser- smur- stau2- sne~ snm~ snt~ sel+ smul+ stau1+ ser+ smur+ stau2+ n2 n3 n4 x2+ x2- str str~ 
   add process p p > x1+ stl~  [QCD] / go sul scl sur scr sdl ssl sbl sdr ssr sbr sul~ scl~ sur~ scr~ sdl~ ssl~ sbl~ sdr~ ssr~ sbr~ sne snm snt sel- smul- stau1- ser- smur- stau2- sne~ snm~ snt~ sel+ smul+ stau1+ ser+ smur+ stau2+ n2 n3 n4 x2+ x2- str str~
'''

decays['1000006'] = """DECAY   1000006    6.85567609E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
"""

# TODO: apparently the width is too small for a particle in s-channel... 1.25982028E-08
decays['1000024'] = """DECAY   1000024     1.25982028E-01   # chargino1+ decays
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
"""

decays['6'] = """DECAY  6   1.320000e+00
   1.000000e+00   2    5  24
"""
#decays['24'] = """DECAY  24   2.085000e+00
#      3.377000e-01   2   -1   2
#      3.377000e-01   2   -3   4
#      1.082000e-01   2  -11  12
#      1.082000e-01   2  -13  14
#      1.082000e-01   2  -15  16
#"""
#decays['25'] = """DECAY 25 6.382339e-03
#"""
decays['13'] = """DECAY 13 0.0
"""
decays['15'] = """DECAY 15 0.0
"""
decays['1000001'] = """DECAY 1000001 0.0 # WsdL 
"""
decays['1000002'] = """DECAY 1000002 0.0 # WsuL 
"""
decays['1000003'] = """DECAY 1000003 0.0 # WssL 
"""
decays['1000004'] = """DECAY 1000004 0.0 # WscL 
"""
decays['1000005'] = """DECAY 1000005 0.0 # WsbL 
"""
decays['1000011'] = """DECAY 1000011 0.0 # WseL 
"""
decays['1000012'] = """DECAY 1000012 0.0 # Wsne 
"""
decays['1000013'] = """DECAY 1000013 0.0 # WsmuL 
"""
decays['1000014'] = """DECAY 1000014 0.0 # Wsnm 
"""
decays['1000015'] = """DECAY 1000015 0.0 # Wstau1 
"""
decays['1000016'] = """DECAY 1000016 0.0 # Wsnt 
"""
decays['1000021'] = """DECAY 1000021 0.0 # Wgo 
"""
decays['1000022'] = """DECAY 1000022 0.0 # Wneu1 
"""
decays['1000023'] = """DECAY 1000023 0.0 # Wneu2 
"""
decays['1000025'] = """DECAY 1000025 0.0 # Wneu3 
"""
decays['1000035'] = """DECAY 1000035 0.0 # Wneu4 
"""
decays['1000037'] = """DECAY 1000037 0.0 # Wch2 
"""
decays['2000001'] = """DECAY 2000001 0.0 # WsdR 
"""
decays['2000002'] = """DECAY 2000002 0.0 # WsuR 
"""
decays['2000003'] = """DECAY 2000003 0.0 # WssR 
"""
decays['2000004'] = """DECAY 2000004 0.0 # WscR 
"""
decays['2000005'] = """DECAY 2000005 0.0 # WsbR 
"""
decays['2000006'] = """DECAY 2000006 0.0 # WstR 
"""
decays['2000011'] = """DECAY 2000011 0.0 # WseR 
"""
decays['2000013'] = """DECAY 2000013 0.0 # WsmuR 
"""
decays['2000015'] = """DECAY 2000015 0.0 # Wstau2 
"""

# TODO: fix these blocks
if 'Ho' in JOName:
   ## trying to force RH t1
   #param_blocks['USQMIX']={}
   #param_blocks['USQMIX']['3 3']='0.00E+00'
   #param_blocks['USQMIX']['3 6']='1.00E+00'
   #param_blocks['USQMIX']['6 3']='1.00E+00'
   #param_blocks['USQMIX']['6 6']='0.00E+00'

   # Higgsino scenario
   param_blocks['VMIX']={}
   param_blocks['VMIX']['1 1']='0.00E+00'
   param_blocks['VMIX']['1 2']='1.00E+00'
   param_blocks['VMIX']['2 1']='1.00E+00'
   param_blocks['VMIX']['2 2']='0.00E+00'

   param_blocks['UMIX']={}
   param_blocks['UMIX']['1 1']='0.00E+00'
   param_blocks['UMIX']['1 2']='1.00E+00'
   param_blocks['UMIX']['2 1']='1.00E+00'
   param_blocks['UMIX']['2 2']='0.00E+00'

   param_blocks['NMIX']={}
   param_blocks['NMIX']['1 1']='0.00E+00'
   param_blocks['NMIX']['1 2']='0.00E+00'
   param_blocks['NMIX']['1 3']='7.07E-01'
   param_blocks['NMIX']['1 4']='-7.07E-01'
   param_blocks['NMIX']['2 1']='0.00E+00'
   param_blocks['NMIX']['2 2']='0.00E+00'
   param_blocks['NMIX']['2 3']='-7.07E-01'
   param_blocks['NMIX']['2 4']='-7.07E-01'
   param_blocks['NMIX']['3 1']='1.00E+00'
   param_blocks['NMIX']['3 2']='0.00E+00'
   param_blocks['NMIX']['3 3']='0.00E+00'
   param_blocks['NMIX']['3 4']='0.00E+00'
   param_blocks['NMIX']['4 1']='0.00E+00'
   param_blocks['NMIX']['4 2']='-1.00E+00'
   param_blocks['NMIX']['4 3']='0.00E+00'
   param_blocks['NMIX']['4 4']='0.00E+00'

elif 'Wo' in JOName:
   ## LH t1
   #param_blocks['USQMIX']={}
   #param_blocks['USQMIX']['3 3']='1.00000000E+00'
   #param_blocks['USQMIX']['3 6']='0.00000000E+00'
   #param_blocks['USQMIX']['6 3']='0.00000000E+00'
   #param_blocks['USQMIX']['6 6']='1.00000000E+00'

   # Wino scenario
   param_blocks['VMIX']={}
   param_blocks['VMIX']['1 1']='1.00E+00'
   param_blocks['VMIX']['1 2']='0.00E+00'
   param_blocks['VMIX']['2 1']='0.00E+00'
   param_blocks['VMIX']['2 2']='0.00E+00'

   param_blocks['UMIX']={}
   param_blocks['UMIX']['1 1']='1.00E+00'
   param_blocks['UMIX']['1 2']='0.00E+00'
   param_blocks['UMIX']['2 1']='0.00E+00'
   param_blocks['UMIX']['2 2']='0.00E+00'

   param_blocks['NMIX']={}
   param_blocks['NMIX']['1 1']='0.00E+00'
   param_blocks['NMIX']['1 2']='1.00E+00'
   param_blocks['NMIX']['1 3']='0.00E+00'
   param_blocks['NMIX']['1 4']='0.00E+00'
   param_blocks['NMIX']['2 1']='1.00E+00'
   param_blocks['NMIX']['2 2']='0.00E+00'
   param_blocks['NMIX']['2 3']='0.00E+00'
   param_blocks['NMIX']['2 4']='0.00E+00'
   param_blocks['NMIX']['3 1']='0.00E+00'
   param_blocks['NMIX']['3 2']='0.00E+00'
   param_blocks['NMIX']['3 3']='0.00E+00'
   param_blocks['NMIX']['3 4']='0.00E+00'
   param_blocks['NMIX']['4 1']='0.00E+00'
   param_blocks['NMIX']['4 2']='0.00E+00'
   param_blocks['NMIX']['4 3']='0.00E+00'
   param_blocks['NMIX']['4 4']='0.00E+00'



######################################################################################

from MadGraphControl.MadGraphUtils import * 

# Set maximum number of events if the event multiplier has been modified
if evt_multiplier>0:
    if runArgs.maxEvents>0:
        nevts=runArgs.maxEvents*evt_multiplier
    else:
        nevts=evgenConfig.nEventsPerJob*evt_multiplier
else:
    # Sensible default
    nevts=evgenConfig.nEventsPerJob*2.
run_settings.update({'nevents':int(nevts)})

# Pass arguments as a dictionary: the "decays" argument is not accepted in older versions of MadGraphControl
if 'mass' in [x.lower() for x in param_blocks]:
    raise RuntimeError('Do not provide masses in param_blocks; use the masses variable instead')
param_blocks['MASS']=masses
# Add decays in if needed
if len(decays)>0: param_blocks['DECAY']=decays

full_proc = SUSY_process(process)
process_dir = new_process(full_proc, plugin=plugin, usePMGSettings=True)
modify_param_card(param_card_input=param_card,process_dir=process_dir,params=param_blocks)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)

#config_setting = {}
#config_setting.update({'output_dependencies':'external'})
#config_setting.update({'ninja':'/afs/cern.ch/work/d/dzanzi/private/SUSY/tt1L/newSignals/dmstop_signal_v3/leftStop/LocalMadGraph/HEPTools/lib'})
#config_setting.update({'collier':'/afs/cern.ch/work/d/dzanzi/private/SUSY/tt1L/newSignals/dmstop_signal_v3/leftStop/LocalMadGraph/HEPTools/lib'})
#config_setting.update({'run_mode':'2'})
#config_setting.update({'nb_core':'None'})
#modify_config_card(process_dir=process_dir,settings=config_setting)

## Cook the setscales file for the user defined dynamical scale
#import fileinput
#fileN = process_dir+'/SubProcesses/setscales.f'
#mark  = '      elseif(dynamical_scale_choice.eq.10) then'
#rmLines = ['ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
#           'cc      USER-DEFINED SCALE: ENTER YOUR CODE HERE                                 cc',
#           'cc      to use this code you must set                                            cc',
#           'cc                 dynamical_scale_choice = 0                                    cc',
#           'cc      in the run_card (run_card.dat)                                           cc',
#           'write(*,*) "User-defined scale not set"',
#           'stop 1',
#           'temp_scale_id=\'User-defined dynamical scale\' ! use a meaningful string',
#           'tmp = 0',
#           'cc      USER-DEFINED SCALE: END OF USER CODE                                     cc'
#           ]
#
#for line in fileinput.input(fileN, inplace=1):
#    toKeep = True
#    for rmLine in rmLines:
#        if line.find(rmLine) >= 0:
#           toKeep = False
#           break
#    if toKeep:
#        print(line),
#    if line.startswith(mark):
#        print("""
#c         Q^2= mt^2 + 0.5*(pt^2+ptbar^2)
#          xm2=dot(pp(0,3),pp(0,3))
#          tmp=sqrt(xm2+0.5*(pt(pp(0,3))**2+pt(pp(0,4))**2))
#          temp_scale_id='mt**2 + 0.5*(pt**2+ptbar**2)'
#              """)



# Set up madspin if needed
if madspin_card is not None:
    import shutil
    if not os.access(madspin_card,os.R_OK):
        raise RuntimeError('Could not locate madspin card at '+str(madspin_card))
    shutil.copy(madspin_card,process_dir+'/Cards/madspin_card.dat')

# Grab the run card and move it into place
generate(runArgs=runArgs,process_dir=process_dir,grid_pack=False)

# Move output files into the appropriate place, with the appropriate name
arrange_output(process_dir=process_dir,saveProcDir=False,runArgs=runArgs,fixEventWeightsForBridgeMode=False)

# Check if we were running multi-core, and if so move back to single core for Pythia8
check_reset_proc_number(opts)

write_test_script()

evgenConfig.contact  = [ "daniele.zanzi@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel','stop']
evgenConfig.description = 'stop+C1 production, st->b+C1 in simplified model'
evgenConfig.keywords += ["SUSY"]

