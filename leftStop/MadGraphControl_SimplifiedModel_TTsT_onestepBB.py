include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()
#get the file name and split it on _ to extract relavent information                                                                                              
jobConfigParts = JOName.split("_")

mstop=float(jobConfigParts[4])
mchargino=float(jobConfigParts[5])
mneutralino=float(jobConfigParts[6].split('.')[0])

#TODO
keepOutput = True

masses['6'] = 172.5  ## TODO: overwrite top mass value of 175GeV in MSSM_SLHA2
masses['24'] = 80.3
masses['1000006'] = mstop
masses['1000024'] = mchargino
masses['1000022'] = mneutralino
if masses['1000022']<0.5: masses['1000022']=0.5

decays['13'] = """DECAY 13 0.0
"""## TODO: set to zero to avoid errors otherwise is too little for schannel 

if '_TT' in JOName and 'lep' in JOName:
   process = '''
   define p = g u c d s b u~ c~ d~ s~ b~
   define j = g u c d s b u~ c~ d~ s~ b~
   generate p p > t1 t1~ / go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ susysl susyv susyv~, (t1 > x1+ b, x1+ > mu+ vm n1 / susysl susyv susyv~ ), (t1~ > x1- b~, x1- > mu- vm~ n1 / susysl susyv susyv~) 
   '''
   flavourScheme=5
elif '_TT' in JOName:
   process = '''
   define p = g u c d s b u~ c~ d~ s~ b~
   define j = g u c d s b u~ c~ d~ s~ b~
   generate p p > t1 t1~ / go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ susysl susyv susyv~
   '''
   flavourScheme=5
elif '_sTTT' in JOName and 'lep' in JOName:
   process = '''
   define p = g u c d s b u~ c~ d~ s~ b~
   define j = g u c d s b u~ c~ d~ s~ b~ 
   generate p p > x1- t1 / go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ susysl susyv susyv~,  (x1- > mu- vm~ n1 / susysl susyv susyv~), (t1  > x1+ b,  x1+ > mu+ vm  n1 / susysl susyv susyv~)  @1
   add process p p > x1+ t1~ / go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ susysl susyv susyv~, (x1+ > mu+ vm  n1 / susysl susyv susyv~), (t1~ > x1- b~, x1- > mu- vm~ n1 / susysl susyv susyv~)  @2
   '''
   flavourScheme=5
elif '_sTTT' in JOName:
   process = '''
   define p = g u c d s b u~ c~ d~ s~ b~
   define j = g u c d s b u~ c~ d~ s~ b~
   generate p p > x1- t1 / go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ susysl susyv susyv~ @1
   add process p p > x1+ t1~ / go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ susysl susyv susyv~ @2
   '''
   flavourScheme=5
else:
   evgenLog.info('ERROR: Process not recognised')
   exit()

decays['1000006'] = """DECAY   1000006     6.85567609E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     0.00000000E+00    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     0.00000000E+00    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     0.00000000E+00    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     0.00000000E+00    2     1000021         6   # BR(~t_1 -> ~g      t )
     0.00000000E+00    2     1000005        37   # BR(~t_1 -> ~b_1    H+)
     0.00000000E+00    2     2000005        37   # BR(~t_1 -> ~b_2    H+)
     0.00000000E+00    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     0.00000000E+00    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
"""

decays['1000024'] = """DECAY   1000024     1.25982028E-08   # chargino1+ decays
     0.00000000E+00    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     0.00000000E+00    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     0.00000000E+00    3     1000025         2        -1   # BR(~chi_1+ -> ~chi_30 u    db)
     0.00000000E+00    3     1000035         2        -1   # BR(~chi_1+ -> ~chi_40 u    db)
     0.00000000E+00    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     0.00000000E+00    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     0.00000000E+00    3     1000025         4        -3   # BR(~chi_1+ -> ~chi_30 c    sb)
     0.00000000E+00    3     1000035         4        -3   # BR(~chi_1+ -> ~chi_40 c    sb)
     0.00000000E+00    3     1000022         6        -5   # BR(~chi_1+ -> ~chi_10 t    bb)
     0.00000000E+00    3     1000023         6        -5   # BR(~chi_1+ -> ~chi_20 t    bb)
     0.00000000E+00    3     1000025         6        -5   # BR(~chi_1+ -> ~chi_30 t    bb)
     0.00000000E+00    3     1000035         6        -5   # BR(~chi_1+ -> ~chi_40 t    bb)
     0.00000000E+00    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     0.00000000E+00    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     0.00000000E+00    3     1000025       -11        12   # BR(~chi_1+ -> ~chi_30 e+   nu_e)
     0.00000000E+00    3     1000035       -11        12   # BR(~chi_1+ -> ~chi_40 e+   nu_e)
     0.00000000E+00    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     0.00000000E+00    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     0.00000000E+00    3     1000025       -13        14   # BR(~chi_1+ -> ~chi_30 mu+  nu_mu)
     0.00000000E+00    3     1000035       -13        14   # BR(~chi_1+ -> ~chi_40 mu+  nu_mu)
     0.00000000E+00    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
     0.00000000E+00    3     1000023       -15        16   # BR(~chi_1+ -> ~chi_20 tau+ nu_tau)
     0.00000000E+00    3     1000025       -15        16   # BR(~chi_1+ -> ~chi_30 tau+ nu_tau)
     0.00000000E+00    3     1000035       -15        16   # BR(~chi_1+ -> ~chi_40 tau+ nu_tau)
     0.00000000E+00    3     1000021         2        -1   # BR(~chi_1+ -> ~g      u    db)
     0.00000000E+00    3     1000021         4        -3   # BR(~chi_1+ -> ~g      c    sb)
     0.00000000E+00    3     1000021         6        -5   # BR(~chi_1+ -> ~g      t    bb)
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
"""

#decays['24'] = """DECAY  24   2.085000e+00
#      1.0   2  -13  14
#"""

if 'Ho' in JOName:
   # trying to force RH t1
   param_blocks['USQMIX']={}
   param_blocks['USQMIX']['3 3']='0.00E+00'
   param_blocks['USQMIX']['3 6']='1.00E+00'
   param_blocks['USQMIX']['6 3']='1.00E+00'
   param_blocks['USQMIX']['6 6']='0.00E+00'
   
   # Higgsino scenario
   param_blocks['VMIX']={}
   param_blocks['VMIX']['1 1']='0.00E+00'
   param_blocks['VMIX']['1 2']='1.00E+00'
   param_blocks['VMIX']['2 1']='1.00E+00'
   param_blocks['VMIX']['2 2']='0.00E+00'
   
   param_blocks['UMIX']={}
   param_blocks['UMIX']['1 1']='0.00E+00'
   param_blocks['UMIX']['1 2']='1.00E+00'
   param_blocks['UMIX']['2 1']='1.00E+00'
   param_blocks['UMIX']['2 2']='0.00E+00'
   
   param_blocks['NMIX']={}
   param_blocks['NMIX']['1 1']='0.00E+00'  
   param_blocks['NMIX']['1 2']='0.00E+00' 
   param_blocks['NMIX']['1 3']='7.07E-01'  
   param_blocks['NMIX']['1 4']='-7.07E-01' 
   param_blocks['NMIX']['2 1']='0.00E+00' 
   param_blocks['NMIX']['2 2']='0.00E+00' 
   param_blocks['NMIX']['2 3']='-7.07E-01'  
   param_blocks['NMIX']['2 4']='-7.07E-01' 
   param_blocks['NMIX']['3 1']='1.00E+00' 
   param_blocks['NMIX']['3 2']='0.00E+00'  
   param_blocks['NMIX']['3 3']='0.00E+00'  
   param_blocks['NMIX']['3 4']='0.00E+00'  
   param_blocks['NMIX']['4 1']='0.00E+00' 
   param_blocks['NMIX']['4 2']='-1.00E+00'  
   param_blocks['NMIX']['4 3']='0.00E+00'  
   param_blocks['NMIX']['4 4']='0.00E+00' 

elif 'Wo' in JOName:
   # LH t1
   param_blocks['USQMIX']={}
   param_blocks['USQMIX']['3 3']='1.00000000E+00'
   param_blocks['USQMIX']['3 6']='0.00000000E+00'
   param_blocks['USQMIX']['6 3']='0.00000000E+00'
   param_blocks['USQMIX']['6 6']='1.00000000E+00'
   
   # Wino scenario
   param_blocks['VMIX']={}
   param_blocks['VMIX']['1 1']='1.00E+00'
   param_blocks['VMIX']['1 2']='0.00E+00'
   param_blocks['VMIX']['2 1']='0.00E+00'
   param_blocks['VMIX']['2 2']='0.00E+00'
   
   param_blocks['UMIX']={}
   param_blocks['UMIX']['1 1']='1.00E+00'
   param_blocks['UMIX']['1 2']='0.00E+00'
   param_blocks['UMIX']['2 1']='0.00E+00'
   param_blocks['UMIX']['2 2']='0.00E+00'
   
   param_blocks['NMIX']={}
   param_blocks['NMIX']['1 1']='0.00E+00'  
   param_blocks['NMIX']['1 2']='1.00E+00' 
   param_blocks['NMIX']['1 3']='0.00E+00' 
   param_blocks['NMIX']['1 4']='0.00E+00' 
   param_blocks['NMIX']['2 1']='1.00E+00' 
   param_blocks['NMIX']['2 2']='0.00E+00' 
   param_blocks['NMIX']['2 3']='0.00E+00' 
   param_blocks['NMIX']['2 4']='0.00E+00' 
   param_blocks['NMIX']['3 1']='0.00E+00' 
   param_blocks['NMIX']['3 2']='0.00E+00'  
   param_blocks['NMIX']['3 3']='0.00E+00'  
   param_blocks['NMIX']['3 4']='0.00E+00'  
   param_blocks['NMIX']['4 1']='0.00E+00' 
   param_blocks['NMIX']['4 2']='0.00E+00' 
   param_blocks['NMIX']['4 3']='0.00E+00' 
   param_blocks['NMIX']['4 4']='0.00E+00' 


evgenLog.info('Registered generation of stop pair production, stop to b+chargino; grid point '+str(JOName)+' decoded into mass point m_stop=' + str(masses['1000006']) + ' GeV, m_C1='+str(masses['1000024']) + ' GeV, m_N1='+str(masses['1000022']) + ' GeV')

#--------------------------------------------------------------
#                      Madspin configuration
#--------------------------------------------------------------
 

#evgenLog.info('Running w/ MadSpin option')
#madspin_card='madspin_card.dat'
#
#mscard = open(madspin_card,'w')
#
#mscard.write("""#************************************************************   
##*                        MadSpin                           *   
##*                                                          *   
##*  P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer   *   
##*                                                          *   
##*         Part of the MadGraph5_aMC@NLO Framework:         *   
##*    The MadGraph5_aMC@NLO Development Team - Find us at   *   
##*    https://server06.fynu.ucl.ac.be/projects/madgraph     *   
##*                                                          *   
##************************************************************   
##Some options (uncomment to apply)  
#set max_weight_ps_point 400  # number of PS to estimate the maximum for each event  
##   
#set seed %i 
## specify the decay for the final state particles   
#define l+ = mu+
#define l- = mu-
#define vl = vm
#define vl~ = vm~
#decay t1 > x1+ b
#decay t1~ > x1- b~
#decay x1+ > n1 W+
#decay x1- > n1 W-
#decay t > W+ b
#decay t~ > W- b~
#decay w+ > l+ vl
#decay w- > l- vl~
##   
##   
## running the actual code       
#launch"""%runArgs.randomSeed)
#
#mscard.close()

evgenConfig.contact  = [ "daniele.zanzi@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel','stop']
evgenConfig.description = 'stop direct pair production, st->b+C1 in simplified model'

genSeq.Pythia8.Commands += [ "Merging:Process = guess" ]
genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

