#!/bin/sh

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

ls

setupATLAS

#asetup AthGeneration,21.6.99,here
asetup 21.6,latest,AthGeneration,here
export PYTHONPATH=/afs/cern.ch/user/d/dzanzi/WORKDIR/SUSY/tt1L/newSignals/dmstop_signal_v3/leftStop:$PYTHONPATH
export MADPATH=/afs/cern.ch/user/d/dzanzi/WORKDIR/SUSY/tt1L/newSignals/dmstop_signal_v3/leftStop/LocalMadGraph
export PYTHONPATH=$MADPATH:$PYTHONPATH
export ATHENA_PROC_NUMBER=4
echo $PYTHONPATH
Gen_tf.py --ecmEnergy=13000.0 --jobConfig=$PWD --outputEVNTFile=tmp.EVNT.root --maxEvents=100 #--rivetAnas=MC_GENERIC,MC_KTSPLITTINGS --outputYODAFile=MyOutput.yoda.gz

#asetup 21.2.147.0,AthDerivation,here
#Reco_tf.py --inputEVNTFile tmp.EVNT.root --outputDAODFile test.pool.root --reductionConf TRUTH3
#
#asetup AnalysisBase,21.2.206
#source /afs/cern.ch/user/d/dzanzi/WORKDIR/SUSY/tt1L/SimpleAnalysis/x86_64-centos7-gcc8-opt/setup.sh
#simpleAnalysis  -a StopOneLepton2018 DAOD_TRUTH3.test.pool.root
#
#cp MyOutput.yoda.gz /afs/cern.ch/user/d/dzanzi/EOS_dzanzi/SUSY/SignalTest/MGPy8EG_A14N_SMsTSTR5F.yoda.gz
#cp tmp.EVNT.root /afs/cern.ch/user/d/dzanzi/EOS_dzanzi/SUSY/SignalTest/MGPy8EG_A14N_SMsTSTR5F.EVNT.root
#cp DAOD_TRUTH3.test.pool.root /afs/cern.ch/user/d/dzanzi/EOS_dzanzi/SUSY/SignalTest/MGPy8EG_A14N_SMsTSTR5F.DAOD_TRUTH3.test.pool.root 
#cp StopOneLepton2018.root /afs/cern.ch/user/d/dzanzi/EOS_dzanzi/SUSY/SignalTest/MGPy8EG_A14N_SMsTSTR5F.StopOneLepton2018.root
#cp StopOneLepton2018.txt /afs/cern.ch/user/d/dzanzi/EOS_dzanzi/SUSY/SignalTest/MGPy8EG_A14N_SMsTSTR5F.StopOneLepton2018.txt
#
#rm tmp.EVNT.root
#rm DAOD_TRUTH3.test.pool.root
#rm events.lhe*
#rm tmp_LHE_events*

