from MadGraphControl.MadGraphUtils import *

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
model_string = phys_short.split('_')[3]
mphi = phys_short.split('_')[4].replace("p","")
mchi = phys_short.split('_')[5].replace("c","")
if "p" in str(mchi):
    mchi = str(mchi).replace("p",".")
mphi = float(mphi)
mchi = float(mchi)

evgenLog.info('Processing model with masses: (mphi, mchi) = (%e,%e)' %(mphi, mchi))

#TODO
keepOutput = True

# Default run settings
run_settings = {
    'event_norm'    : 'average',
    'lhe_version'   : '3.0',
    'ickkw'         : 0,
    'parton_shower' : 'PYTHIA8',
    'req_acc'       : '0.001',
    'bwcutoff'      : '50',
    'dynamical_scale_choice' : '3', 
} 

from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

evt_multiplier=1.2
if evt_multiplier>0:
    if runArgs.maxEvents>0:
        nevents=runArgs.maxEvents*evt_multiplier
    else:
        nevents=evgenConfig.nEventsPerJob*evt_multiplier

run_settings.update({'nevents':int(nevents)})

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")


plugin=None
if "ttscalar" in phys_short.lower() or "ttpseudo" in phys_short.lower():
    gen_process = """
import model DMsimp_s_spin0 --modelname
define p = g u c d s u~ c~ d~ s~ b b~ 
define j = g u c d s u~ c~ d~ s~ b b~ 
generate p p > xd xd~ t t~ / a z w+ w- [QCD]
output -f
"""
elif "twscalar" in phys_short.lower() or "twpseudo" in phys_short.lower():
    gen_process = """
import model DMsimp_s_spin0_5f_ybMSbar --modelname
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
generate p p > xd xd~ t w- [QCD]
add process p p > xd xd~ t~ w+ [QCD]
output -f
"""
#    plugin='MadSTR'
#    # TODO: set these properly
#    run_settings.update({
#      'istr': '2',
#      'str_include_pdf':  'True',
#      'str_include_flux': 'True',
#    })

# Set up the process
process_dir = new_process(gen_process, plugin=plugin, usePMGSettings=True)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)

# this is a dictionary
params = {}
params["dminputs"] = {}
params["dminputs"]["gSXd"] = 0.0
params["dminputs"]["gSd11"] = 0.0
params["dminputs"]["gSu11"] = 0.0
params["dminputs"]["gSd22"] = 0.0
params["dminputs"]["gSu22"] = 0.0
params["dminputs"]["gSd33"] = 0.0
params["dminputs"]["gSu33"] = 0.0
params["dminputs"]["gPXd"] = 0.0
params["dminputs"]["gPd11"] = 0.0
params["dminputs"]["gPu11"] = 0.0
params["dminputs"]["gPd22"] = 0.0
params["dminputs"]["gPu22"] = 0.0
params["dminputs"]["gPd33"] = 0.0
params["dminputs"]["gPu33"] = 0.0
params["dminputs"]["gSh1"] = 0.0
params["dminputs"]["gSb"] = 0.0

params['MASS']={'1000022':mchi,'54':mphi}
params['DECAY']={'54':'DECAY  54 Auto # WY0'}
params['DECAY'].update({
'13': 'DECAY 13 0.0',
'15': 'DECAY 15 0.0',
})


if "scalar" in phys_short.lower():
    params["dminputs"]["gSXd"] = 1.0
    params["dminputs"]["gSd33"] = 1.0
    params["dminputs"]["gSu33"] = 1.0
elif "pseudo" in phys_short.lower():
    params["dminputs"]["gPXd"] = 1.0
    params["dminputs"]["gPd33"] = 1.0
    params["dminputs"]["gPu33"] = 1.0

modify_param_card(process_dir=process_dir,params=params)



# Generate the events
generate(runArgs=runArgs,process_dir=process_dir,grid_pack=False)

arrange_output(process_dir=process_dir,saveProcDir=True,runArgs=runArgs,fixEventWeightsForBridgeMode=False)

genSeq.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10",
                            "1000022:all = Chi Chi~ 1 0 0 %s " %mchi,
                           ]

check_reset_proc_number(opts)

evgenConfig.description = 'DM+%s at NLO, m_med = %s GeV, m_chi = %s GeV'%(model_string, mphi, mchi)
evgenConfig.keywords = ["exotic","BSM","WIMP", "SUSY"]
evgenConfig.contact = ["Claudia Seitz <claudia.seitz@cern.ch>, Priscilla Pani <ppani@cern.ch>, Michaela Queitsch-Maitland <michaela.queitsch-maitland@cern.ch>"]
