#!/bin/sh

#DSID=100000
#
#while read p; do
#
#  rm -r ${DSID}
#  mkdir ${DSID}
#  cd ${DSID}
#
#  cp ../MadGraphControl_SimplifiedModel_TTsT_onestepBB.py .
#  cp ../run_EVT_stop.sh .
#
#  echo "include( 'MadGraphControl_SimplifiedModel_TTsT_onestepBB.py' )" > ${p}
#
#  old_p=${p}
#  p="${p/mc./}"
#  p="${p/.py/}"
#
#  sed -i "s/ToBeReplaced/${p}/g" run_EVT_stop.sh
#
#  echo "executable = run_EVT_stop.sh" >> ${p}".sub"
#  echo "should_transfer_files = YES" >> ${p}".sub"
#  echo "transfer_input_files = MadGraphControl_SimplifiedModel_TTsT_onestepBB.py,${old_p}" >> ${p}".sub"
#  echo "output          = "${p}".\$(ClusterId).\$(ProcId).out" >> ${p}".sub"
#  echo "error           = "${p}".\$(ClusterId).\$(ProcId).err" >> ${p}".sub"
#  echo "log             = "${p}".\$(ClusterId).\$(ProcId).log" >> ${p}".sub"
#  echo "universe = vanilla" >> ${p}".sub"
#  echo '+JobFlavour      = "workday"' >> ${p}".sub" # workday=8h, tomorrow=1day, testmatch=3days
#  echo "queue" >> ${p}".sub"
#
#  echo "JOB "${p}" "${p}".sub\n" >> ${p}".dag"
#
#  condor_submit_dag -force ${p}.dag
#
#  cd ..
#
#  DSID=$((DSID+1))
#
#done < newStopSignals.txt
#
#DSID=100000
#
#rm -r prepare_stop_JOs
#mkdir prepare_stop_JOs
#cd prepare_stop_JOs
#
#while read p; do
#
#  mkdir ${DSID}
#  cd ${DSID}
#
#  if [ "${DSID}" -eq "100000" ]; then
#     cp ../../MadGraphControl_SimplifiedModel_TTsT_onestepBB.py .
#  else
#     ln -s ../100000/MadGraphControl_SimplifiedModel_TTsT_onestepBB.py . 
#  fi
#
#  echo "include( 'MadGraphControl_SimplifiedModel_TTsT_onestepBB.py' )" > ${p}
#
#  cd ..
#  DSID=$((DSID+1))
#
#done < ../newStopSignals.txt
#
#cd ..
#
#rm prepare_stop_JOs.tar.gz 
#tar czf prepare_stop_JOs.tar.gz  prepare_stop_JOs
#
#DSID=200000
#
#while read p; do
#
#  rm -r ${DSID}
#  mkdir ${DSID}
#  cd ${DSID}
#
#  cp ../MadGraphControl_SimplifiedModel_TTNLO_onestepBB.py .
#  cp ../run_EVT_stop.sh .
#
#  echo "include( 'MadGraphControl_SimplifiedModel_TTNLO_onestepBB.py' )" > ${p}
#
#  old_p=${p}
#  p="${p/mc./}"
#  p="${p/.py/}"
#
#  sed -i "s/ToBeReplaced/${p}/g" run_EVT_stop.sh
#
#  echo "executable = run_EVT_stop.sh" >> ${p}".sub"
#  echo "should_transfer_files = YES" >> ${p}".sub"
#  echo "transfer_input_files = MadGraphControl_SimplifiedModel_TTNLO_onestepBB.py,${old_p}" >> ${p}".sub"
#  echo "output          = "${p}".\$(ClusterId).\$(ProcId).out" >> ${p}".sub"
#  echo "error           = "${p}".\$(ClusterId).\$(ProcId).err" >> ${p}".sub"
#  echo "log             = "${p}".\$(ClusterId).\$(ProcId).log" >> ${p}".sub"
#  echo "universe = vanilla" >> ${p}".sub"
#  echo '+JobFlavour      = "workday"' >> ${p}".sub" # workday=8h, tomorrow=1day, testmatch=3days
#  echo "queue" >> ${p}".sub"
#
#  echo "JOB "${p}" "${p}".sub\n" >> ${p}".dag"
#
#  condor_submit_dag -force ${p}.dag
#
#  cd ..
#
#  DSID=$((DSID+1))
#
#done < newStopSignalsNLO.txt
#
#
#DSID=200000
#
#rm -r prepare_stopNLO_JOs
#mkdir prepare_stopNLO_JOs
#cd prepare_stopNLO_JOs
#
#while read p; do
#
#  mkdir ${DSID}
#  cd ${DSID}
#
#  if [ "${DSID}" -eq "100000" ]; then
#     cp ../../MadGraphControl_SimplifiedModel_TTNLO_onestepBB.py .
#  else
#     ln -s ../100000/MadGraphControl_SimplifiedModel_TTNLO_onestepBB.py . 
#  fi
#
#  echo "include( 'MadGraphControl_SimplifiedModel_TTNLO_onestepBB.py' )" > ${p}
#
#  cd ..
#  DSID=$((DSID+1))
#
#done < ../newStopSignalsNLO.txt
#
#cd ..
#
#rm prepare_stopNLO_JOs.tar.gz
#tar czf prepare_stopNLO_JOs.tar.gz  prepare_stopNLO_JOs
#
#DSID=300000
#
#while read p; do
#
#  rm -r ${DSID}
#  mkdir ${DSID}
#  cd ${DSID}
#
#  cp ../MadGraphControl_SimplifiedModel_sTNLO_onestepB.py .
#  cp ../run_EVT_stop.sh .
#  echo "include( 'MadGraphControl_SimplifiedModel_sTNLO_onestepB.py' )" > ${p}
#
#  old_p=${p}
#  p="${p/mc./}"
#  p="${p/.py/}"
#
#  sed -i "s/ToBeReplaced/${p}/g" run_EVT_stop.sh
#
#  echo "executable = run_EVT_stop.sh" >> ${p}".sub"
#  echo "should_transfer_files = YES" >> ${p}".sub"
#  echo "transfer_input_files = MadGraphControl_SimplifiedModel_sTNLO_onestepB.py,${old_p}" >> ${p}".sub"
#  echo "output          = "${p}".\$(ClusterId).\$(ProcId).out" >> ${p}".sub"
#  echo "error           = "${p}".\$(ClusterId).\$(ProcId).err" >> ${p}".sub"
#  echo "log             = "${p}".\$(ClusterId).\$(ProcId).log" >> ${p}".sub"
#  echo "universe = vanilla" >> ${p}".sub"
#  echo '+JobFlavour      = "workday"' >> ${p}".sub" # workday=8h, tomorrow=1day, testmatch=3days
#  echo "queue" >> ${p}".sub"
#
#  echo "JOB "${p}" "${p}".sub\n" >> ${p}".dag"
#
#  condor_submit_dag -force ${p}.dag
#
#  cd ..
#
#  DSID=$((DSID+1))
#
#done < newStopSignalsNLO_singlestop.txt
#
#DSID=300000
#
#rm -r prepare_singleStopNLO_JOs
#mkdir prepare_singleStopNLO_JOs
#cd prepare_singleStopNLO_JOs
#
#while read p; do
#
#  mkdir ${DSID}
#  cd ${DSID}
#
#  if [ "${DSID}" -eq "100000" ]; then
#     cp ../../MadGraphControl_SimplifiedModel_sTNLO_onestepB.py .
#  else
#     ln -s ../100000/MadGraphControl_SimplifiedModel_sTNLO_onestepB.py . 
#  fi
#
#  echo "include( 'MadGraphControl_SimplifiedModel_sTNLO_onestepB.py' )" > ${p}
#
#  cd ..
#  DSID=$((DSID+1))
#
#done < ../newStopSignalsNLO_singlestop.txt
#
#cd ..
#
#rm prepare_singleStopNLO_JOs.tar.gz
#tar czf prepare_singleStopNLO_JOs.tar.gz  prepare_singleStopNLO_JOs


#DM

DSID=400000

while read p; do

  rm -r ${DSID}
  mkdir ${DSID}
  cd ${DSID}

  cp ../MadGraphControl_DMSimp_DM_TT_TW_TJ.py .
  cp ../run_EVT_stop.sh .

  echo "include( 'MadGraphControl_DMSimp_DM_TT_TW_TJ.py' )" > ${p}

  old_p=${p}
  p="${p/mc./}"
  p="${p/.py/}"

  sed -i "s/ToBeReplaced/${p}/g" run_EVT_stop.sh

  echo "executable = run_EVT_stop.sh" >> ${p}".sub"
  echo "should_transfer_files = YES" >> ${p}".sub"
  echo "transfer_input_files = MadGraphControl_DMSimp_DM_TT_TW_TJ.py,${old_p}" >> ${p}".sub"
  echo "output          = "${p}".\$(ClusterId).\$(ProcId).out" >> ${p}".sub"
  echo "error           = "${p}".\$(ClusterId).\$(ProcId).err" >> ${p}".sub"
  echo "log             = "${p}".\$(ClusterId).\$(ProcId).log" >> ${p}".sub"
  echo "universe = vanilla" >> ${p}".sub"
  echo '+JobFlavour      = "workday"' >> ${p}".sub" # workday=8h, tomorrow=1day, testmatch=3days
  echo "queue" >> ${p}".sub"

  echo "JOB "${p}" "${p}".sub\n" >> ${p}".dag"

  #condor_submit_dag -force ${p}.dag

  cd ..

  DSID=$((DSID+1))

done < newDMSignals.txt

DSID=400000

rm -r prepare_DM_JOs
mkdir prepare_DM_JOs
cd prepare_DM_JOs

while read p; do

  mkdir ${DSID}
  cd ${DSID}

  if [ "${DSID}" -eq "100000" ]; then
     cp ../../MadGraphControl_DMSimp_DM_TT_TW_TJ.py .
  else
     ln -s ../100000/MadGraphControl_DMSimp_DM_TT_TW_TJ.py . 
  fi

  echo "include( 'MadGraphControl_DMSimp_DM_TT_TW_TJ.py' )" > ${p}

  cd ..
  DSID=$((DSID+1))

done < ../newDMSignals.txt

cd ..

rm prepare_DM_JOs.tar.gz
tar czf prepare_DM_JOs.tar.gz  prepare_DM_JOs


DSID=500000

while read p; do

  rm -r ${DSID}
  mkdir ${DSID}
  cd ${DSID}

  cp ../MadGraphControl_DMSimp_DM_TT_TW_TJ_NLO.py .
  cp ../run_EVT_stop.sh .

  echo "include( 'MadGraphControl_DMSimp_DM_TT_TW_TJ_NLO.py' )" > ${p}

  old_p=${p}
  p="${p/mc./}"
  p="${p/.py/}"

  sed -i "s/ToBeReplaced/${p}/g" run_EVT_stop.sh

  echo "executable = run_EVT_stop.sh" >> ${p}".sub"
  echo "should_transfer_files = YES" >> ${p}".sub"
  echo "transfer_input_files = MadGraphControl_DMSimp_DM_TT_TW_TJ_NLO.py,${old_p}" >> ${p}".sub"
  echo "output          = "${p}".\$(ClusterId).\$(ProcId).out" >> ${p}".sub"
  echo "error           = "${p}".\$(ClusterId).\$(ProcId).err" >> ${p}".sub"
  echo "log             = "${p}".\$(ClusterId).\$(ProcId).log" >> ${p}".sub"
  echo "universe = vanilla" >> ${p}".sub"
  echo '+JobFlavour      = "workday"' >> ${p}".sub" # workday=8h, tomorrow=1day, testmatch=3days
  echo "queue" >> ${p}".sub"

  echo "JOB "${p}" "${p}".sub\n" >> ${p}".dag"

  #condor_submit_dag -force ${p}.dag

  cd ..

  DSID=$((DSID+1))

done < newDMSignals.txt

DSID=500000

rm -r prepare_DMNLO_JOs
mkdir prepare_DMNLO_JOs
cd prepare_DMNLO_JOs

while read p; do

  mkdir ${DSID}
  cd ${DSID}

  if [ "${DSID}" -eq "100000" ]; then
     cp ../../MadGraphControl_DMSimp_DM_TT_TW_TJ_NLO.py .
  else
     ln -s ../100000/MadGraphControl_DMSimp_DM_TT_TW_TJ_NLO.py . 
  fi

  echo "include( 'MadGraphControl_DMSimp_DM_TT_TW_TJ_NLO.py' )" > ${p}

  cd ..
  DSID=$((DSID+1))

done < ../newDMSignals.txt

cd ..

rm prepare_DMNLO_JOs.tar.gz
tar czf prepare_DMNLO_JOs.tar.gz  prepare_DMNLO_JOs
