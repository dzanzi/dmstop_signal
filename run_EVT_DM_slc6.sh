#!/bin/sh

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

ls

export ALRB_CONT_RUNPAYLOAD="source slc6_wrapper_DM.sh"
setupATLAS -c slc6

#setupATLAS
#asetup AthGeneration,21.6.97,here
#Gen_tf.py --ecmEnergy=13000.0 --jobConfig=$PWD --outputEVNTFile=tmp.EVNT.root --maxEvents=20000 --rivetAnas=MC_GENERIC,MC_KTSPLITTINGS --outputYODAFile=MyOutput.yoda.gz

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS
asetup 21.2.160.0,AthDerivation,here

hadd tmp.EVNT.root tmp.EVNT.*.root

Reco_tf.py --inputEVNTFile tmp.EVNT.0.root tmp.EVNT.1.root tmp.EVNT.2.root tmp.EVNT.3.root --outputDAODFile test.pool.root --reductionConf TRUTH3

asetup AnalysisBase,21.2.156
source /afs/cern.ch/user/d/dzanzi/WORKDIR/SUSY/tt1L/SimpleAnalysis/build/x86_64-centos7-gcc8-opt/setup.sh 
simpleAnalysis -s layout=run2 -a StopOneLepton2020  DAOD_TRUTH3.test.pool.root


cp tmp.EVNT.root /afs/cern.ch/user/d/dzanzi/EOS_dzanzi/SUSY/SignalTest/ToBeReplaced.EVNT.root
cp DAOD_TRUTH3.test.pool.root /afs/cern.ch/user/d/dzanzi/EOS_dzanzi/SUSY/SignalTest/ToBeReplaced.DAOD_TRUTH3.test.pool.root 
cp StopOneLepton2020.root /afs/cern.ch/user/d/dzanzi/EOS_dzanzi/SUSY/SignalTest/ToBeReplaced.StopOneLepton2020.root

rm tmp.EVNT.*.root
rm tmp.EVNT.root
rm DAOD_TRUTH3.test.pool.root
rm events.lhe*
rm tmp_LHE_events*

