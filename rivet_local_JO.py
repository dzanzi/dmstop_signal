theApp.EvtMax = 10000

#my_sample='mc15_13TeV.503462.MGPy8EG_A14N23LO_TT_directTT_500_200.evgen.EVNT.e8258'
#my_sample='mc15_13TeV.503466.MGPy8EG_A14N23LO_TT_directTT_500_325.evgen.EVNT.e8258'
#my_sample='mc15_13TeV.503484.MGPy8EG_A14N23LO_TT_directTT_700_350.evgen.EVNT.e8258'
#my_sample='mc15_13TeV.503489.MGPy8EG_A14N23LO_TT_directTT_700_525.evgen.EVNT.e8258'
#my_sample='mc15_13TeV.503504.MGPy8EG_A14N23LO_TT_directTT_900_400.evgen.EVNT.e8258'
#my_sample='mc15_13TeV.503507.MGPy8EG_A14N23LO_TT_directTT_900_700.evgen.EVNT.e8258'
#my_sample='mc15_13TeV.503520.MGPy8EG_A14N23LO_TT_directTT_1100_1.evgen.EVNT.e8258'
#my_sample='mc15_13TeV.503523.MGPy8EG_A14N23LO_TT_directTT_1100_600.evgen.EVNT.e8258'
#my_sample='mc15_13TeV.503542.MGPy8EG_A14N23LO_TT_directTT_1400_1.evgen.EVNT.e8258'
my_sample='mc15_13TeV.503544.MGPy8EG_A14N23LO_TT_directTT_1400_400.evgen.EVNT.e8258'

import AthenaPoolCnvSvc.ReadAthenaPool
import glob
input_coll = glob.glob(my_sample+'/*.root*');
svcMgr.EventSelector.InputCollections = input_coll
#svcMgr.EventSelector.InputCollections = [ '/eos/atlas/atlascerngroupdisk/phys-gener/examples/rivet/EVNT.root' ]

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()
import os
rivet.AnalysisPath = os.environ['PWD']

rivet.Analyses += [ 'MC_GENERIC','MC_KTSPLITTINGS' ]
rivet.RunName = ''
rivet.HistoFile = my_sample+'.yoda.gz'
rivet.CrossSection = 1.0
#rivet.IgnoreBeamCheck = True
rivet.SkipWeights=True
job += rivet

